package me.riggah.autorespawn;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import net.minecraft.server.v1_7_R1.EnumClientCommand;
import net.minecraft.server.v1_7_R1.PacketPlayInClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class AutoRespawn extends JavaPlugin implements Listener
{
	final static Logger log = Bukkit.getLogger();
	
	@Override
	public void onEnable()
	{
		this.getServer().getPluginManager().registerEvents(this, this);
		log.info("AutoRespawn has been enabled!");
		log.info("AutoRespawn v" + this.getDescription().getVersion() + " by riggah");
		
	}
	
	@Override
	public void onDisable()
	{
		log.info("AutoRespawn has been disabled!");
	}
	
	@EventHandler
	public void onRespawn(PlayerDeathEvent ev)
	{
		final Player plr = ev.getEntity();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable()
		{

			@Override
			public void run() {
				
				PacketPlayInClientCommand packet = new PacketPlayInClientCommand();
				try {
					Field a = PacketPlayInClientCommand.class.getDeclaredField("a");
					a.setAccessible(true);
					a.set(packet, EnumClientCommand.PERFORM_RESPAWN);
				} catch (NoSuchFieldException e) {
					
					e.printStackTrace();
				} catch (SecurityException e) {
					
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					
					e.printStackTrace();
				}
				((CraftPlayer) plr).getHandle().playerConnection.a(packet);
				log.info(plr.getName() + " respawned automatically!");
			}
			
		}, 2L);
	}

}
